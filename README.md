We provide several examples to demonstrate the method for the reconstruction of rational functions described in e-Print: 2306.12262. The paper is here https://arxiv.org/pdf/2306.12262.pdf.

For each example, we provide Mathematica readable files:
 - "topology_definition", containing the definition of the integral topology;
 - "master_integrals", containing the list of master integrals;
 - "target_integral", containing the target integral;
 - "reduction_coefficients", containing the list of reduction coefficients for the target integral in terms of master integrals;
 - "linear_system", containing the linear system satisfied by the reduction coefficients in conjunction with the auxiliary function f(x) = 1 over a finite field, specified by the prime number provided in "topology_definition".

For example (a) and (b), the target integral is introduced in the paper. For example (c) and (d), the target integral is defined as the derivative of the first master integral with respect to s12 and mtsq, respectively.